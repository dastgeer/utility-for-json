import json
import time
import re
from pprint import pprint
#import translate
import time
import re
from googletrans import Translator



## translating the word to english and returns the translated word with " "
def translateToEnglish(word):
    return '"'+Translator().translate(word, dest='en').text+'"'

##detects and return the laguage of the word
def langDetect(word):
    return Translator().detect(word).lang

if __name__ == '__main__':
    startTime = time.time()
    #fetch the json file
    #dirPath = "C:/Users/Mayur/PycharmProjects/nestedDict/"
    dirPath = "C:/Users/Mayur/Documents/Vertiv/JSON Example_files/"
    #C:\Users\Mayur\PycharmProjects\nestedDict\who\czech.json
    #FileName = 'output.json'
    #FileName = 'DB05606.json'
    #FileName = 'DB13966.json'
    FileName = 'DB13927.json'

    sourceFile = dirPath + FileName

    TransEngWord = {}
    OrgLangWord = {}

    ##Translated output json file
    output_file = dirPath + 'DB13927_T.json'

    with open(output_file, 'w+') as of:

        with open(sourceFile, encoding="utf-8") as i_f:
            for line in i_f:
                chars = list(line)
                i =0
                while i < len(chars):
                    if chars[i] != '"':
                        #print(chars[i],end="")
                        try:
                            of.write('{}'.format(chars[i]))
                        except:
                            temp = chars[i].encoding('utf-8-sig')
                            of.write('{}'.format(temp))
                        #pass
                    else:

                        i += 1
                        nonEnglish_start = i
                        flag = 1        #flag = 1 for values
                        while i < len(chars) and chars[i:i+2] not in( ['"',':'], ['"','\n'], ['"',',']):

                            i+= 1

                        if chars[i:i + 2] == ['"', ':']:
                            flag = 0  # flag = 0 for keys



                        nonEnglish_end= i

                        word = ''.join(chars[nonEnglish_start : nonEnglish_end])

                        eng_word = ''

                        try:
                            OrgLangWord[word] = langDetect(word)
                        except :
                            #print("Can't Recognize the language of word -", word)
                            OrgLangWord[word] = ''

                        if flag == 0 or OrgLangWord[word] == 'en':           #if word is key they take it as it is
                            eng_word = '"'+word+'"'

                        elif word not in TransEngWord and OrgLangWord[word] != 'en':  #if word is value and non english then translate it into English
                            eng_word = translateToEnglish(word)
                            TransEngWord[word] = eng_word

                        else:           #if word is in TransEngWord directory then take the translated word from directory
                            eng_word = TransEngWord[word]

                        #print(eng_word, end="")
                        try:
                            of.write('{}'.format(eng_word))
                        except:
                            eng_word = eng_word.encode('utf-8-sig')
                              of.write('{}'.format(eng_word))



                    #print(chars[i], end="")
                    i+=1

    of.close()


    print()
    print('TransEngWord Dictionary length: ', len(TransEngWord))
    print('TransEngWord Dictionary :')
    print(TransEngWord)

    print()
    print('OrgLangWord Dictionary length: ', len(OrgLangWord))
    print('OrgLangWord Dictionary :')
    print(OrgLangWord)