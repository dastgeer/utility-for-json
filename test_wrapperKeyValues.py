import pytest
from wrapperKeyValues_02 import json_translator


@pytest.mark.parametrize("element, expected", [('हिन्दी', 'Hindi'), ('中文词', 'Chinese words'),('Bitte',"You're welcome")])
def test_translateToEnglish(element, expected):
    jt = json_translator()
    assert jt.translateToEnglish(element) == expected