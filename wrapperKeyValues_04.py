from googletrans import Translator
import os
import time

class json_translator:
    ## translating the word to english and returns the translated values
    @staticmethod
    def translateToEnglish(word):
        return Translator().translate(word, dest='en').text

    ##detects and return the laguage of the word
    @staticmethod
    def langDetect(word):
        return Translator().detect(word).lang

    def json_translate(self, dir, translateKeys):
        #path_to_jsons = dir
        json_files = [pos_json for pos_json in os.listdir(dir) if pos_json.endswith('.json')]
        print('##########################################')
        print('All JSON Files')
        print('##########################################')
        print(json_files)  # for me this prints ['foo.json', 'foo_T.json']

        nontranslated_jsons = [pos_json for pos_json in os.listdir(dir) if pos_json.endswith('.json') and pos_json[-7:] != '_T.json']#[json for json in json_files if json[-7:] != '_T.json' ]
        print('##########################################')
        print('Non Translated JSON Files')
        print('##########################################')
        print(nontranslated_jsons)

        TransEngWord = {}
        OrgLangWord = {}
        translateKeys = set(translateKeys)

        nonEngKeys = {}

        for json_file in nontranslated_jsons:
            sourceFile = dir + json_file
            ##Translated output json file
            output_file = dir + json_file[:-5] +'_T.json'

            with open(output_file, 'w+') as o_file:

                with open(sourceFile, encoding="utf-8") as i_file:
                    for line in i_file:
                        chars = list(line)
                        i = 0
                        while i < len(chars):
                            if chars[i] != '"':
                                # print(chars[i],end="")
                                try:
                                    o_file.write('{}'.format(chars[i]))
                                except:
                                    try:
                                        temp = chars[i].encoding('utf-8-sig')
                                        o_file.write('{}'.format(temp))
                                    except:
                                        try:
                                            temp = chars[i].encoding('utf-16')
                                            o_file.write('{}'.format(temp))
                                        except Exception as e:
                                            print("Error- {} - can't encode the char - {}".format(e, temp))


                                # pass
                            else:

                                i += 1
                                word_start = i
                                flag = 1  # flag = 1 for values
                                while i < len(chars) and chars[i:i + 2] not in (['"', ':'], ['"', '\n'], ['"', ',']):
                                    i += 1

                                if chars[i:i + 2] == ['"', ':']:
                                    flag = 0  # flag = 0 for keys

                                word_end = i

                                word = ''.join(chars[word_start: word_end])

                                eng_word = ''

                                try:
                                    OrgLangWord[word] = json_translator.langDetect(word)
                                except:
                                    # print("Can't Recognize the language of word -", word)
                                    OrgLangWord[word] = ''

                                if flag == 0:  # if word is key they take it as it is
                                    eng_word = word
                                    key = word          #use this key to identify and translate the business key-values

                                elif word not in TransEngWord:   # if word is value and non english then translate it into English
                                    if key in translateKeys and OrgLangWord[word] != 'en':
                                        nonEngKeys[key] = OrgLangWord[word]
                                        eng_word = json_translator.translateToEnglish(word)
                                        TransEngWord[word] = eng_word
                                    else:
                                        eng_word = word

                                else:  # if word is in TransEngWord directory then take the translated word from directory
                                    eng_word = TransEngWord[word]

                                # print(eng_word, end="")
                                try:
                                    o_file.write('"{}"'.format(eng_word))
                                except:
                                    try:
                                        eng_word = eng_word.encode('utf-8-sig')
                                        o_file.write('"{}"'.format(eng_word))
                                    except:
                                        try:
                                            eng_word = eng_word.encode('utf-16')
                                            o_file.write('"{}"'.format(eng_word))
                                        except Exception as e:
                                            print("Error- {} - can't encode the word - {}".format(e, eng_word))


                            # print(chars[i], end="")
                            i += 1

            o_file.close()

            print()
            print('TransEngWord Dictionary length: ', len(TransEngWord))
            print('TransEngWord Dictionary :')
            print(TransEngWord)

            print()
            print('OrgLangWord Dictionary length: ', len(OrgLangWord))
            print('OrgLangWord Dictionary :')
            print(OrgLangWord)

            print()
            print('nonEnglish Keys list length: ', len(OrgLangWord))
            print('non English List :')
            print(nonEngKeys)

            print('#######################################################')
            print('{} got transfered...'.format(json_file))
            print('#######################################################')

    #find keys are English or Not in derectory
    def nonEnglishKeys(self, dir, translateKeys):

        nontranslated_jsons = [pos_json for pos_json in os.listdir(dir) if pos_json.endswith('.json') and pos_json[-7:] != '_T.json' and pos_json != 'keys.json']  # [json for json in json_files if json[-7:] != '_T.json' ]
        print('##########################################')
        print('Non Translated JSON Files')

        print(nontranslated_jsons)
        print('#######################################################')

        TransEngWord = {}
        OrgLangWord = {}
        translateKeys = set(translateKeys)

        #isKeyEnglish = {}
        for json_file in nontranslated_jsons:
            self.nonEnglishKeys_json(dir,json_file)

        print('#######################################################')

    #checking each JSON whether Keys are English or Not
    def nonEnglishKeys_json(self,dir, json_file):
        sourceFile = dir + json_file

        ##Translated output json file
        output_file = dir + '/json/keys/{}_keys.json'.format(json_file[:-5])
        isKeyEnglish = {}

        if os.path.exists(output_file):
            append_write = 'a'  # append if already exists
        else:
            append_write = 'w+'  # make a new file if not

        with open(output_file, append_write) as o_file:
            o_file.write("{\n")

            try:
                with open(sourceFile, encoding="utf-8") as i_file:
                    for line in i_file:
                        chars = list(line)
                        i = 0
                        while i < len(chars):
                            if chars[i] != '"':
                                pass
                            else:
                                i += 1
                                word_start = i
                                flag = 1  # flag = 1 for values
                                while i < len(chars) and chars[i:i + 2] not in (['"', ':'], ['"', '\n'], ['"', ',']):
                                    i += 1

                                if chars[i:i + 2] == ['"', ':']:
                                    flag = 0  # flag = 0 for keys

                                word_end = i

                                word = ''.join(chars[word_start: word_end])

                                eng_word = ''

                                if flag == 0:  # if word is key they take it as it is
                                    # eng_word = word
                                    key = word  # use this key to identify and translate the business key-values

                                # elif word not in TransEngWord:   # if word is value and non english then translate it into English
                                elif key in translateKeys:
                                    if json_translator.langDetect(word) != 'en':
                                        # isKeyEnglish[key] = False
                                        o_file.write("\"{}\":{}\n".format(key, 'false'))

                                    else:
                                        # isKeyEnglish[key] = 'Yes'
                                        o_file.write("\"{}\":{}\n".format(key, 'true'))
                            # print(chars[i], end="")
                            i += 1
            except Exception as e:
                print('{} - Error in file- {}'.format(e, json_file))
            o_file.write("}")

            o_file.close()


        print('{} checked...'.format(json_file))
        #print('#######################################################')



if __name__ == '__main__':
    start = time.time()
    jt = json_translator()
    dir = 'C:/Users/Mayur Sonawane/medmeme/data/2018/03/29/structured/processed/pubmed/json/'
    #dir = 'C:/Users/Mayur Sonawane/Documents/NXSFT/Documents/Pubmed/2018/2018/03/29/structured/processed/pubmed/json/'
    translateKeys = {'Author', 'Article Title', 'AbstractText', 'text', 'Title', 'PubDate', 'ValidYN', 'LastName',
                     'Forename', 'Initials', 'PublicationType'}
    jt.nonEnglishKeys(dir, translateKeys)
    end = time.time()
    time_taken = end - start
    print("Time taken {} secs.".format(time_taken))