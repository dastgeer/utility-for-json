from googletrans import Translator
import os

class json_translator:
    ## translating the word to english and returns the translated word with " "
    @staticmethod
    def translateToEnglish(word):
        return Translator().translate(word, dest='en').text

    ##detects and return the laguage of the word
    @staticmethod
    def langDetect(word):
        return Translator().detect(word).lang

    def json_translate(self, dir):
        #path_to_jsons = dir
        json_files = [pos_json for pos_json in os.listdir(dir) if pos_json.endswith('.json')]
        print('##########################################')
        print('All JSON Files')
        print('##########################################')
        print(json_files)  # for me this prints ['foo.json', 'foo_T.json']

        nontranslated_jsons = [pos_json for pos_json in os.listdir(dir) if pos_json.endswith('.json') and pos_json[-7:] != '_T.json']#[json for json in json_files if json[-7:] != '_T.json' ]
        print('##########################################')
        print('Non Translated JSON Files')
        print('##########################################')
        print(nontranslated_jsons)

        TransEngWord = {}
        OrgLangWord = {}

        for json_file in nontranslated_jsons:
            sourceFile = dir + json_file
            ##Translated output json file
            output_file = dir + json_file[:-5] +'_T.json'

            with open(output_file, 'w+') as o_file:

                with open(sourceFile, encoding="utf-8") as i_file:
                    for line in i_file:
                        chars = list(line)
                        i = 0
                        while i < len(chars):
                            if chars[i] != '"':
                                # print(chars[i],end="")
                                try:
                                    o_file.write('{}'.format(chars[i]))
                                except:
                                    try:
                                        temp = chars[i].encoding('utf-8-sig')
                                        o_file.write('{}'.format(temp))
                                    except:
                                        try:
                                            temp = chars[i].encoding('utf-16')
                                            o_file.write('{}'.format(temp))
                                        except Exception as e:
                                            print("Error- {} - can't encode the char - {}".format(e, temp))


                                # pass
                            else:

                                i += 1
                                nonEnglish_start = i
                                flag = 1  # flag = 1 for values
                                while i < len(chars) and chars[i:i + 2] not in (['"', ':'], ['"', '\n'], ['"', ',']):
                                    i += 1

                                if chars[i:i + 2] == ['"', ':']:
                                    flag = 0  # flag = 0 for keys

                                nonEnglish_end = i

                                word = ''.join(chars[nonEnglish_start: nonEnglish_end])

                                eng_word = ''

                                try:
                                    OrgLangWord[word] = json_translator.langDetect(word)
                                except:
                                    # print("Can't Recognize the language of word -", word)
                                    OrgLangWord[word] = ''

                                if flag == 0 or OrgLangWord[word] == 'en':  # if word is key they take it as it is
                                    eng_word = word

                                elif word not in TransEngWord and OrgLangWord[
                                    word] != 'en':  # if word is value and non english then translate it into English
                                    eng_word = json_translator.translateToEnglish(word)
                                    TransEngWord[word] = eng_word

                                else:  # if word is in TransEngWord directory then take the translated word from directory
                                    eng_word = TransEngWord[word]

                                # print(eng_word, end="")
                                try:
                                    o_file.write('"{}"'.format(eng_word))
                                except:
                                    try:
                                        eng_word = eng_word.encode('utf-8-sig')
                                        o_file.write('"{}"'.format(eng_word))
                                    except:
                                        try:
                                            eng_word = eng_word.encode('utf-16')
                                            o_file.write('"{}"'.format(eng_word))
                                        except Exception as e:
                                            print("Error- {} - can't encode the word - {}".format(e, eng_word))


                            # print(chars[i], end="")
                            i += 1

            o_file.close()

            print()
            print('TransEngWord Dictionary length: ', len(TransEngWord))
            print('TransEngWord Dictionary :')
            print(TransEngWord)

            print()
            print('OrgLangWord Dictionary length: ', len(OrgLangWord))
            print('OrgLangWord Dictionary :')
            print(OrgLangWord)

            print('#######################################################')
            print('{} got transfered...'.format(json_file))
            print('#######################################################')





if __name__ == '__main__':
    jt = json_translator()
    dir = 'C:/Users/Mayur/Documents/Vertiv/JSON Example_files/'
    jt.json_translate(dir)